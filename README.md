# vowo.io Api-Docs

Api specifications using [OpenAPI](https://swagger.io/) v3 for vowo.io.

## Editing

```bash
docker pull swaggerapi/swagger-editor
docker run -d -p 80:8080 swaggerapi/swagger-editor
```

Simply drag and drop the swagger JSON or YAML document into the Swagger Editor browser window.
